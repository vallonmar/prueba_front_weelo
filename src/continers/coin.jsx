import React, { Component } from 'react';
import { Row, Col } from 'react-grid-system';
import NumberFormat from 'react-number-format';
import Select from 'react-select';
import { Redirect } from 'react-router-dom';

import '../style/main.sass';
import axios from 'axios'
import Tabla from '../components/tabla'
import Menu from '../components/menu'
import colmCoins from '../components/columnsCoins'

const colourStyles = {
    control: styles => ({ ...styles,
        margin : '0.2rem',
        with: '100%'
    }),
}
class Home extends Component {

    constructor() {
        super();
        this.state = {
        }
    }
    componentDidMount = () => {
        var coinID = this.props.location.search
        coinID = coinID.replace('?', '')
        coinID = coinID.split('/')
        var id = coinID[0]
        this.detailCoin(id)
        this.markers(id)
        this.social(id)
    }

    detailCoin = (id) => {
        axios.get(`https://api.coinlore.net/api/ticker/?id=${id}`).then((res) => {
            console.log(res)
            if(res.status == 200){
                var coins = res.data
                var name = res.data[0].name 
                var valor = res.data[0].price_usd
                var  columnsCoins = colmCoins(this.detail)
                this.setState({coins, columnsCoins, name, valor})
            } else {
                alert('error')
            }
        });

    }
    detail = (coin) => {
    }
    markers = (id) => {
        axios.get(` https://api.coinlore.net/api/coin/markets/?id=${id}`).then((res) => {
            console.log(res)
            if(res.status == 200){
                var Markers = res.data
                var data = JSON.stringify(Markers[0])
                data = data.replace(/[&\/\\#+[()$~%'"*?<>{}]/g, '')
                var columnsMarkers = [{}]
                var count = 0
                var newdata = data.split(',')
                for(var i = 0; i< newdata.length; i++){
                    var aux = newdata[i].split(':')
                        columnsMarkers[count] = {Header : aux[0], accessor: aux[0]}
                        count = count + 1
                }
                this.setState({columnsMarkers, Markers})
                console.log(columnsMarkers, Markers)
            } else {
                alert('error')
            }
        });

    }
    social = (id) => {
        axios.get(`https://api.coinlore.net/api/coin/social_stats/?id=${id}`).then((res) => {
            this.setState({ social : res.data })            
        });
    }
    render() {
        const {columnsCoins, coins,name,  columnsMarkers, Markers, social, valor} = this.state
        console.log(social)
        return (
            <div>
                <div className='menu'>
                    <Menu page={'coin'} />
                </div>
                <div className='contenedor'> 
                    <div className='data'>
                        <Row>
                            <Col xs={10}>
                                <div className='title'>{name}</div>
                                { coins ?
                                    <Tabla columns={columnsCoins} data={coins} />
                                    : ''
                                }
                                <div className='title'>Markets For Coin</div>

                                { Markers ?
                                    <Tabla columns={columnsMarkers} data={Markers} />
                                    : ''
                                }
                            </Col>
                            <Col xs={2}>
                                <div className='moneda'>{name}  <NumberFormat value={(valor)} displayType={'text'} prefix={'$'} thousandSeparator={true}  renderText={value => <span>{value}</span> }/></div>
                                { social ?
                                    <div className='social'>
                                        <div className='card'>
                                            <b>Reddit</b>
                                            <p> Usuarios Acitvos : <NumberFormat value={(social.reddit.avg_active_users)} displayType={'text'} thousandSeparator={true}  renderText={value => <span>{value}</span>} /> </p>
                                            <p> Suscriptores : <NumberFormat value={(social.reddit.subscribers)} displayType={'text'} thousandSeparator={true}  renderText={value => <span>{value}</span>} /> </p>
                                        </div>
                                        <div className='card'>
                                            <b>Twitter</b>
                                            <p> Followers : <NumberFormat value={(social.twitter.followers_count)} displayType={'text'} thousandSeparator={true}  renderText={value => <span>{value}</span>} /> </p>
                                            <p> Status : <NumberFormat value={(social.twitter.status_count)} displayType={'text'} thousandSeparator={true}  renderText={value => <span>{value}</span>} /> </p>
                                        </div>
                                    </div>
                                    : ''
                                }
                            </Col>
                        </Row>
                        
                    </div>
                </div>
            </div>
        )
    }
}
export default Home;
