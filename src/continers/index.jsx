import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { Row, Col } from 'react-grid-system';
import NumberFormat from 'react-number-format';
import Select from 'react-select';

import '../style/main.sass';
import axios from 'axios'
import Tabla from '../components/tabla'
import Coin from './coin'
import Menu from '../components/menu'
import colmCoins from '../components/columnsCoins'

const colourStyles = {
    control: styles => ({ ...styles,
        margin : '0.2rem',
        with: '100%'
    }),
}
class Home extends Component {

    constructor() {
        super();
        this.state = {
            general: true,
            detalle:false
        }
    }
    componentDidMount = () => {
        this.globalData()
        this.coins()
    }
    globalData = () => {
        axios.get('https://api.coinlore.net/api/global/').then((res) => {
            console.log(res)
            if(res.status == 200){
                var datos = res.data
                var data = JSON.stringify(datos)
                data = data.replace(/[&\/\\#+[()$~%'"*?<>{}]/g, '')
                var newArray = [{}]
                var count = 0
                var newdata = data.split(',')
                for(var i = 0; i< newdata.length; i++){
                    var aux = newdata[i].split(':')
                    newArray[count] = {Header : aux[0], accessor: aux[0]}
                    count = count + 1
                }
                this.setState({ newArray , datos})
            } else {
                alert('error')
            }
        }); 
    }
    coins = () => {
        this.columnsCoins()
        axios.get('https://api.coinlore.net/api/tickers/').then((res) => {
            console.log(res)
            if(res.status == 200){
                var coins = res.data.data
                this.setState({coins})
                console.log(coins)
            } else {
                alert('error')
            }
        });
    }
    columnsCoins = () => {
        var  columnsCoins = colmCoins(this.detail, true)
        this.setState({ columnsCoins })
    }
    detail = (coin) => {
        this.setState({ general : false, coin, detalle: true })
    }
    render() {
        const {newArray , datos, columnsCoins, coins, general, coin, detalle} = this.state
        if (detalle) return <Redirect to={{ pathname: "/coin", search:`${coin.id}`}} />;
        console.log(newArray, datos)
        return (
            <div>
                <div className='menu'>
                    <Menu page={'global'} />
                </div>
                    <div className='contenedor'> 
                        <div className='data'>
                            <div className='title'>Gloabal Data</div>
                            { datos ?
                                <Tabla columns={newArray} data={datos} />
                                : 'No hay datos'
                            }
                            <div className='title'>Coins</div>
                            { coins && general?
                                <Tabla columns={columnsCoins} data={coins} />
                                : ''
                            }
                            { detalle ?
                            <Coin coin={coin} />    
                            : ''
                            }
                        </div>
                </div>
            </div>
        )
    }
}
export default Home;
