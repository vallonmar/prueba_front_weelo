import NumberFormat from 'react-number-format';

const colmCoins = (detail, estado) => {
    const columnsCoins = [{
        Header: 'Rank',
        accessor: 'rank',
        
    },{
        Header: 'Name',
        accessor: 'name',
        Cell: props =>{
            return <div style={{ cursor : 'pointer' }} onClick={() => { detail(props.row.original)}} >{props.row.original.name} - {props.row.original.symbol}</div>
        }
    },{
        Header: 'price_usd',
        accessor: 'price_usd',
        Cell: props =>{
            return <NumberFormat value={(props.row.original.price_usd)} displayType={'text'} thousandSeparator={true} prefix={'$'} renderText={value => <p>{value}</p>} />
        }
    },{
        Header: '1h %',
        accessor: 'percent_change_1h',
        Cell: props =>{
            return <div style={{ color: props.row.original.percent_change_1h < 0 ? 'red' : '#2adb2a'}} >{props.row.original.percent_change_1h < 0 ? '▼' : '▲'}{props.row.original.percent_change_1h}</div>
        }
    },{
        Header: '24h %',
        accessor: 'percent_change_24h',
        Cell: props =>{
            return <div style={{ color: props.row.original.percent_change_24h < 0 ? 'red' : '#2adb2a'}} >{props.row.original.percent_change_24h < 0 ? '▼' : '▲'}{props.row.original.percent_change_24h}</div>
        }
    },{
        Header: '7d %',
        accessor: 'percent_change_7d',
        Cell: props =>{
            return <div style={{ color: props.row.original.percent_change_7d < 0 ? 'red' : '#2adb2a'}} >{props.row.original.percent_change_7d < 0 ? '▼' : '▲'}{props.row.original.percent_change_7d}</div>
        }
    },{
        Header: 'price_btc	',
        accessor: 'price_btc	',
        Cell: props =>{
            return <NumberFormat value={(props.row.original.price_btc	)} displayType={'text'} thousandSeparator={true} prefix={'$'} renderText={value => <p>{value}</p>} />
        }
    },{
        Header: 'market_cap_usd	',
        accessor: 'market_cap_usd	',
        Cell: props =>{
            return <NumberFormat value={(props.row.original.market_cap_usd	)} displayType={'text'} thousandSeparator={true} prefix={'$'} renderText={value => <p>{value}</p>} />
        }
    },{
        Header: 'volume24	',
        accessor: 'volume24	',
        Cell: props =>{
            return <NumberFormat value={(props.row.original.volume24	)} displayType={'text'} thousandSeparator={true} prefix={'$'} renderText={value => <p>{value}</p>} />
        }
    },{
        Header: 'msupply	',
        accessor: 'msupply	',
        Cell: props =>{
            return <NumberFormat value={(props.row.original.msupply	)} displayType={'text'} thousandSeparator={true} prefix={'$'} renderText={value => <p>{value}</p>} />
        }
    }]
    return columnsCoins
}
export default colmCoins;