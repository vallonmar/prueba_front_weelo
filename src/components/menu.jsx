import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { Row, Col } from 'react-flexbox-grid';
import Select from 'react-select';

import '../style/main.sass';

const colourStyles = {
    control: styles => ({ ...styles,
        margin : '0.2rem',
    }),
    option: (styles) => {
        return {
            ...styles,
            textTransform: 'lowercase',
        }
    },
    menuPortal: base => ({ ...base, zIndex: 9999 })
    
}

class Home extends Component {

    constructor() {
        super();
        this.state = {
            date : new Date()
        }
    }

    componentDidMount = () => {
        this.setState({ page: this.props.page })
    }

    render() {
        const { page, date } = this.state
        return (
            <div className='posicion'>
                <div className='logo'>
                </div>
                
                <div className='list2'>
                    <ul>
                        <li className={page === 'global' ? 'activo' : ''}><i className="fas fa-globe icono"></i><Link className='menuLis' to={'/'}>Global Data</Link></li>
                        <li className={page === 'market' ? 'activo' : ''}><i className="fas fa-funnel-dollar icono"></i><Link className='menuLis' to={'/markets'}>Markets</Link></li>
                    </ul>
                    
                </div>
               
                <div className='hora'>
                    <ul>
                        <li className='text'></li>
                    </ul>
                </div>
            </div>
        )
    }
}
export default Home;
