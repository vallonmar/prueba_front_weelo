import React, { Component } from 'react';
import { Row, Col } from 'react-flexbox-grid';
import { useTable, useSortBy, useFilters, useGlobalFilter, useAsyncDebounce } from 'react-table';

import '../style/main.sass';

// Define a default UI for filtering
  function Table({ columns, data }) {
    const {
      getTableProps,
      getTableBodyProps,
      headerGroups,
      rows,
      prepareRow,
      visibleColumns,
    } = useTable(
      {
        columns,
        data,
      },
      useGlobalFilter, // useGlobalFilter!
      useSortBy,

    )
  
    // We don't want to render all of the rows for this example, so cap
    // it for this use case
    const firstPageRows = rows.slice(0, 30)
  
    return (
      <div >
        <table width='100%' style={{ borderCollapse: 'collapse'  }} {...getTableProps()} >
          <thead>
          <tr>
            </tr>
            {headerGroups.map(headerGroup => (
              <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map(column => (
                  <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                    {column.render('Header')}
                    {/* Render the columns filter UI */}
                    <span>
                  </span>
                  </th>
                ))}
              </tr>
            ))}
            
          </thead>
          <tbody  {...getTableBodyProps()}>
            {firstPageRows.map((row, i) => {
              prepareRow(row)
              return (
                <tr {...row.getRowProps()}>
                  {row.cells.map(cell => {
                    return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                  })}
                </tr>
              )
            })}
          </tbody>
        </table>
        <br />
      </div>
    )
  }
  

class Usuarios extends Component {

    constructor() {
        super();
        this.state = {
            data:[]
        }
    }
    render() {
        return (
            <div className='tablaData'>
                <Table width="100"  columns={this.props.columns} data={this.props.data} />
            </div>
        )
    }
}
export default Usuarios;
