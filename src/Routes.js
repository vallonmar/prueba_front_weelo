import React from "react";
import {
    BrowserRouter as Router,
    Route,
    Switch,
} from "react-router-dom"
import Home from './continers'
import Coin from './continers/coin'
import Markets from './continers/markets'

const Rutas = () => (
    <Router>
        <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/coin" exact component={Coin} />
            <Route path="/markets" exact component={Markets} />
        </Switch>
    </Router>
);

export default Rutas;